#include <vector>
#include <algorithm>
#include <iostream>

bool SortIt(int a, int b) {
	return a < b;
}
int main() {
	std::vector<int> ReportsOfErr(10);
	std::vector<int> ReportsAllErr(5);
	std::vector<int> ReportsDeltaErr(0);

	srand(time(0));
	std::generate(ReportsOfErr.begin(), ReportsOfErr.end(), []()->int { return int(float(rand()) / float(RAND_MAX) * 10); });

	for (auto i : ReportsOfErr) {
		std::cout << i << " ";
	}

	std::partial_sort_copy(ReportsOfErr.begin(), ReportsOfErr.end(), ReportsAllErr.begin(), ReportsAllErr.end(), SortIt);
	std::cout << std::endl;
	for (auto i : ReportsAllErr) {
		std::cout << i << " ";
	}

	for (auto i : ReportsDeltaErr) {
		std::cout << i << " ";
	}
}